// find users with first name and last name with letter a and hide the id
db.users.find(
	{
		$or:
		[
			{"firstName" : {$regex: "A", $options: "i"}},
			{"lastName" : {$regex: "A", $options: "i"}}
		]
	}, 
            {"firstName" : 1, "lastName": 1, "_id": 0}
);



// find users who are admins and is active
db.users.find(
	{
		$and: 
		[
			{"isAdmin" : true}, 
			{"isActive" : true} 
		]
	}
);


// find courses with letter u and price gte to 13000 (use $regex and $gte)
db.courses.find(
	
			{
				"firstName" : {$regex: "U", $options: "i"}
				"price" : {$gte : 13000.00}
			}
);

// for checking
db.users.find()
db.courses.find()